 \renewcommand*{\probch}{0} \setcounter{probd}{0}
 \renewcommand*{\probch}{1} \setcounter{probd}{0}
\section*{Problems for Chapter~1}
\begin{probdeferred}{1.1}
Prove that there is at most one empty set, i.e., show that if $A$ and $B$
are sets without !!{element}s, then $A = B$.
\end{probdeferred}
\begin{probdeferred}{1.2}
List all subsets of $\{a, b, c, d\}$.
\end{probdeferred}
\begin{probdeferred}{1.2}
Show that if $A$ has $n$ !!{element}s, then $\Pow{A}$ has $2^n$
!!{element}s.
\end{probdeferred}
\begin{probdeferred}{1.4}
Prove that if $A \subseteq B$, then $A \cup B = B$.
\end{probdeferred}
\begin{probdeferred}{1.4}
Prove rigorously that if $A \subseteq B$, then $A \cap B = A$.
\end{probdeferred}
\begin{probdeferred}{1.4}
	Show that if $A$ is a set and $A \in B$, then $A \subseteq \bigcup B$.
\end{probdeferred}
\begin{probdeferred}{1.4}
	Prove that if $A \subsetneq B$, then $B \setminus A \neq \emptyset$.
\end{probdeferred}
\begin{probdeferred}{1.5}
	Using \olref[sfr][set][pai]{wienerkuratowski}, prove that $\tuple{a,
	b}= \tuple{c, d}$ iff both $a = c$ and $b=d$.
\end{probdeferred}
\begin{probdeferred}{1.5}
List all !!{element}s of $\{1, 2, 3\}^3$.
\end{probdeferred}
\begin{probdeferred}{1.5}
Show, by induction on~$k$, that for all $k \ge 1$, if $A$ has $n$
!!{element}s, then $A^k$ has $n^k$ !!{element}s.
\end{probdeferred}
 \renewcommand*{\probch}{2} \setcounter{probd}{0}
\section*{Problems for Chapter~2}
\begin{probdeferred}{2.1}
  List the !!{element}s of the relation $\subseteq$ on the set
  $\Pow{\{a, b, c\}}$.
\end{probdeferred}
\begin{probdeferred}{2.2}
Give examples of relations that are (a) reflexive and symmetric but
not transitive, (b) reflexive and anti-symmetric, (c) anti-symmetric,
transitive, but not reflexive, and (d) reflexive, symmetric, and
transitive.  Do not use relations on numbers or sets.
\end{probdeferred}
\begin{probdeferred}{2.3}
Show that $\equiv_n$ is an equivalence relation, for any $n \in
\Nat$, and that $\equivclass{\Nat}{\equiv_n}$ has exactly $n$ members.
\end{probdeferred}
\begin{probdeferred}{2.4}
Give a proof of \olref[sfr][rel][ord]{prop:partialtostrict}.
\end{probdeferred}
\begin{probdeferred}{2.5}
  Consider the less-than-or-equal-to relation~$\le$ on the set $\{1,
  2, 3, 4\}$ as a graph and draw the corresponding diagram.
\end{probdeferred}
\begin{probdeferred}{2.6}
Show that the transitive closure of $R$ is in fact transitive.
\end{probdeferred}
 \renewcommand*{\probch}{3} \setcounter{probd}{0}
\section*{Problems for Chapter~3}
\begin{probdeferred}{3.4}
Prove \olref[sfr][fun][inv]{prop:bijection-inverse}. That is, show that if
$f\colon A \to B$ is !!{bijective}, an inverse $g$ of $f$ exists. You
have to define such a $g$, show that it is a function, and show that
it is an inverse of~$f$, i.e., $f(g(y)) = y$ and $g(f(x)) = x$ for all
$x \in A$ and $y \in B$.
\end{probdeferred}
\begin{probdeferred}{3.4}
Show that if $f\colon A \to B$ has an inverse~$g$, then $f$ is
!!{bijective}.
\end{probdeferred}
\begin{probdeferred}{3.4}
Prove \olref[sfr][fun][inv]{prop:inverse-unique}. That is, show that
if $g\colon B \to A$ and $g'\colon B \to A$ are inverses of~$f\colon A
\to B$, then $g = g'$, i.e., for all $y \in B$, $g(y) = g'(y)$.
\end{probdeferred}
\begin{probdeferred}{3.5}
Show that if $f \colon A \to B$ and $g \colon B \to C$ are both
!!{injective}, then $\comp{f}{g}\colon A \to C$ is !!{injective}.
\end{probdeferred}
\begin{probdeferred}{3.5}
Show that if $f \colon A \to B$ and $g \colon B \to C$ are both
!!{surjective}, then $\comp{f}{g}\colon A \to C$ is !!{surjective}.
\end{probdeferred}
\begin{probdeferred}{3.5}
Suppose $f \colon A \to B$ and $g \colon B \to C$. Show that the graph
of $\comp{f}{g}$ is $R_f \mid R_g$.
\end{probdeferred}
\begin{probdeferred}{3.6}
Given $f\colon A \pto B$, define the partial function $g\colon B \pto
A$ by: for any $y \in B$, if there is a unique $x \in A$ such that
$f(x) = y$, then $g(y) = x$; otherwise $g(y) \fundefined$.  Show that
if $f$ is injective, then $g(f(x)) = x$ for all $x \in \dom{f}$, and
$f(g(y)) = y$ for all $y \in \ran{f}$.
\end{probdeferred}
 \renewcommand*{\probch}{4} \setcounter{probd}{0}
\section*{Problems for Chapter~4}
\begin{probdeferred}{4.2}
Define an enumeration of the positive squares $1$, $4$, $9$, $16$, \dots
\end{probdeferred}
\begin{probdeferred}{4.2}
  Show that if $A$ and $B$ are !!{enumerable}, so is $A \cup B$. To do
  this, suppose there are !!{surjective} functions $f\colon \PosInt \to
  A$ and $g\colon \PosInt \to B$, and define !!a{surjective}
  function~$h\colon \PosInt \to A \cup B$ and prove that it is
  !!{surjective}. Also consider the cases where $A$ or~$B = \emptyset$.
\end{probdeferred}
\begin{probdeferred}{4.2}
  Show that if $B \subseteq A$ and $A$ is !!{enumerable}, so is~$B$. To
  do this, suppose there is !!a{surjective} function $f\colon \PosInt \to
  A$. Define !!a{surjective} function~$g\colon \PosInt \to B$ and prove
  that it is !!{surjective}. What happens if $B = \emptyset$?
\end{probdeferred}
\begin{probdeferred}{4.2}
  Show by induction on $n$ that if $A_1$, $A_2$, \dots, $A_n$ are all
  !!{enumerable}, so is $A_1 \cup \dots \cup A_n$. You may assume the
  fact that if two sets $A$ and~$B$ are !!{enumerable}, so is~$A \cup
  B$.
\end{probdeferred}
\begin{probdeferred}{4.2}
  According to \olref[sfr][siz][enm]{defn:enumerable}, a set $A$ is
  enumerable iff $A = \emptyset$ or there is !!a{surjective} $f\colon
  \PosInt \to A$.  It is also possible to define ``!!{enumerable} set''
  precisely by: a set is enumerable iff there is !!a{injective}
  function $g\colon A \to \PosInt$.  Show that the definitions are
  equivalent, i.e., show that there is !!a{injective} function
  $g\colon A \to \PosInt$ iff either $A = \emptyset$ or there is
  !!a{surjective} $f\colon \PosInt \to A$.
\end{probdeferred}
\begin{probdeferred}{4.4}
Give an enumeration of the set of all non-negative rational numbers.
\end{probdeferred}
\begin{probdeferred}{4.4}
Show that $\Rat$ is !!{enumerable}. Recall that any rational number
can be written as a fraction $z/m$ with $z \in \Int$, $m \in \Nat^+$.
\end{probdeferred}
\begin{probdeferred}{4.4}
Define an enumeration of $\Bin^*$.
\end{probdeferred}
\begin{probdeferred}{4.4}
Recall from your introductory logic course that each possible truth
table expresses a truth function. In other words, the truth functions
are all functions from $\Bin^k \to \Bin$ for some~$k$. Prove that the
set of all truth functions is enumerable.
\end{probdeferred}
\begin{probdeferred}{4.4}
Show that the set of all finite subsets of an arbitrary infinite
!!{enumerable} set is !!{enumerable}.
\end{probdeferred}
\begin{probdeferred}{4.4}
A subset of $\Nat$ is said to be \emph{cofinite} iff it is the
complement of a finite set $\Nat$; that is, $A \subseteq \Nat$ is
cofinite iff $\Nat\setminus A$ is finite. Let $I$ be the set whose
!!{element}s are exactly the finite and cofinite subsets of $\Nat$.
Show that $I$ is !!{enumerable}.
\end{probdeferred}
\begin{probdeferred}{4.4}
Show that the !!{enumerable} union of !!{enumerable} sets is
!!{enumerable}. That is, whenever $A_1$, $A_2$, \dots{} are sets, and
each $A_i$ is !!{enumerable}, then the union $\bigcup_{i=1}^\infty
A_i$ of all of them is also !!{enumerable}. [NB: this is hard!]
\end{probdeferred}
\begin{probdeferred}{4.4}
Let $f \colon A \times B \to \Nat$ be an arbitrary pairing function.
Show that the inverse of $f$ is an enumeration of $A \times B$.
\end{probdeferred}
\begin{probdeferred}{4.4}
Specify a function that encodes $\Nat^3$.
\end{probdeferred}
\begin{probdeferred}{4.6}
Show that $\Pow{\Nat}$ is !!{nonenumerable} by a diagonal argument.
\end{probdeferred}
\begin{probdeferred}{4.6}
Show that the set of functions $f \colon \PosInt \to \PosInt$ is
!!{nonenumerable} by an explicit diagonal argument. That is, show that
if $f_1$, $f_2$, \dots, is a list of functions and each $f_i\colon
\PosInt \to \PosInt$, then there is some $\overline{f}\colon \PosInt \to
\PosInt$ not on this list.
\end{probdeferred}
\begin{probdeferred}{4.7}
Show that if there is an !!{injective} function $g\colon B \to A$, and
$B$~is !!{nonenumerable}, then so is~$A$. Do this by showing how you
can use~$g$ to turn an enumeration of~$A$ into one of~$B$.
\end{probdeferred}
\begin{probdeferred}{4.7}
Show that the set of all \emph{sets of} pairs of positive integers is
!!{nonenumerable} by a reduction argument.
\end{probdeferred}
\begin{probdeferred}{4.7}
Show that $\Nat^\omega$, the set of infinite sequences of
natural numbers, is !!{nonenumerable} by a reduction argument.
\end{probdeferred}
\begin{probdeferred}{4.7}
Let $P$ be the set of functions from the set of positive
integers to the set $\{0\}$, and let $Q$ be the set of \emph{partial}
functions from the set of positive integers to the set $\{0\}$. Show
that $P$~is !!{enumerable} and $Q$~is not. (Hint: reduce the problem
of enumerating $\Bin^\omega$ to enumerating~$Q$).
\end{probdeferred}
\begin{probdeferred}{4.7}
Let $S$ be the set of all !!{surjective} functions from the set of
positive integers to the set \{0,1\}, i.e., $S$ consists of all
!!{surjective}~$f\colon \PosInt \to \Bin$.  Show that $S$ is
!!{nonenumerable}.
\end{probdeferred}
\begin{probdeferred}{4.7}
Show that the set~$\Real$ of all real numbers is !!{nonenumerable}.
\end{probdeferred}
\begin{probdeferred}{4.8}
Show that if $\cardeq{A}{C}$ and $\cardeq{B}{D}$, and $A \cap B =
C \cap D = \emptyset$, then $\cardeq{A \cup B}{C \cup D}$.
\end{probdeferred}
\begin{probdeferred}{4.8}
Show that if $A$ is infinite and !!{enumerable}, then
$\cardeq{A}{\Nat}$.
\end{probdeferred}
\begin{probdeferred}{4.9}
  Show that there cannot be !!a{injection} $g\colon \Pow{A} \to
  A$, for any set~$A$. Hint: Suppose $g\colon \Pow{A} \to A$ is
  !!{injective}. Consider $D = \Setabs{g(B)}{B \subseteq A \text{ and
  } g(B) \notin B}$. Let $x = g(D)$. Use the fact that $g$ is
  !!{injective} to derive a contradiction.
\end{probdeferred}
 \renewcommand*{\probch}{5} \setcounter{probd}{0}
\section*{Problems for Chapter~5}
\begin{probdeferred}{5.4}
Prove \olref[fol][syn][unq]{lem:no-prefix}.
\end{probdeferred}
\begin{probdeferred}{5.4}
Prove \olref[fol][syn][unq]{prop:unique-atomic} (Hint: Formulate and
prove a version of \olref[fol][syn][unq]{lem:no-prefix} for terms.)
\end{probdeferred}
\begin{probdeferred}{5.7}
Give an inductive definition of the bound variable occurrences along
the lines of \olref[fol][syn][fvs]{defn:free-occ}.
\end{probdeferred}
\begin{probdeferred}{5.10}
Is $\Struct N$, the standard model of arithmetic, covered? Explain.
\end{probdeferred}
\begin{probdeferred}{5.11}
Let $\Lang L = \{c, f, A\}$ with one !!{constant}, one one-place
!!{function} and one two-place !!{predicate}, and let the
!!{structure} $\Struct{M}$ be given by
\begin{enumerate}
\item $\Domain M = \{1, 2, 3\}$
\item $\Assign{c}{M} = 3$
\item $\Assign{f}{M}(1) = 2, \Assign{f}{M}(2) = 3, \Assign{f}{M}(3) = 2$
\item $\Assign{A}{M} = \{\tuple{1, 2}, \tuple{2, 3}, \tuple{3, 3}\}$
\end{enumerate}
(a) Let $s(v) = 1$ for all variables~$v$.  Find out whether
\[
\Sat{M}{\lexists[x][(A(f(z), c) \lif \lforall[y][(A(y, x) \lor A(f(y),
      x))])]}[s]
\]
Explain why or why not.

(b) Give a different structure and variable assignment in which the
formula is not satisfied.
\end{probdeferred}
\begin{probdeferredtag}{5.12}
{probNot,probOr,probAnd,probIf,probIff,probEx,probAll}
Complete the proof of \olref[fol][syn][ass]{prop:satindep}.
\end{probdeferredtag}
\begin{probdeferred}{5.12}
Prove \olref[fol][syn][ass]{prop:sentence-sat-true}
\end{probdeferred}
\begin{probdeferred}{5.12}
Prove \olref[fol][syn][ass]{prop:sat-quant}.
\end{probdeferred}
\begin{probdeferred}{5.12}
\DeclareRobustCommand{\VDash}{\mathrel{||}\joinrel\Relbar}
Suppose $\Lang L$ is a language without !!{function}s. Given a
!!{structure}~$\Struct M$, $c$ a !!{constant} and $a \in \Domain M$,
define $\Struct M[a/c]$ to be the !!{structure} that is just
like~$\Struct M$, except that $\Assign{c}{M[a/c]} = a$. Define
$\Struct M \VDash !A$ for !!{sentence}s~$!A$ by:
\begin{enumerate}
\tagitem{prvFalse}{%
  \indcase{!A}{\lfalse}{not $\Struct{M} \VDash \indfrm$.}}{}

\tagitem{prvTrue}{%
  \indcase{!A}{\ltrue}{$\Struct{M} \VDash \indfrm$.}}{}

\item \indcase{!A}{\Atom{R}{d_1, \dots, d_n}}{$\Struct M \VDash \indfrm$
  iff $\langle \Assign{d_1}{M}, \dots, \Assign{d_n}{M} \rangle \in
  \Assign{R}{M}$.}

\item \indcase{!A}{\eq[d_1][d_2]}{$\Struct M \VDash \indfrm$ iff
  $\Assign{d_1}{M} = \Assign{d_2}{M}$.}

\tagitem{prvNot}{%
  \indcase{!A}{\lnot !B}{$\Struct{M} \VDash \indfrm$ iff
    not $\Struct{M} \VDash {!B}$.}}{}

\tagitem{prvAnd}{%
  \indcase{!A}{(!B \land !C)}{$\Struct{M} \VDash
    \indfrm$ iff $\Struct{M} \VDash!B$ and $\Struct{M} \VDash !C$.}}{}

\tagitem{prvOr}{%
  \indcase{!A}{(!B \lor !C)}{$\Struct{M} \VDash \indfrm$ iff
    $\Struct{M} \VDash !B$ or $\Struct{M} \VDash !C$ (or both).}}{}

\tagitem{prvIf}{%
  \indcase{!A}{(!B \lif !C)}{$\Struct{M} \VDash \indfrm$ iff
    not $\Struct {M} \VDash !B$ or $\Struct M \VDash !C$ (or both).}}{}

\tagitem{prvIff}{%
  \indcase{!A}{(!B \liff !C)}{$\Struct{M} \VDash {\indfrm}$ iff either
    both $\Struct{M} \VDash {!B}$ and $\Struct{M} \VDash {!C}$, or
    neither $\Struct{M} \VDash {!B}$ nor $\Struct{M} \VDash {!C}$.}}{}

\tagitem{prvAll}{%
  \indcase{!A}{\lforall[x][!B]}{$\Struct{M} \VDash {\indfrm}$ iff for
    all $a \in \Domain{M}$, $\Struct{M[a/c]} \VDash \Subst{!B}{c}{x}$,
    if $c$ does not occur in~$!B$.}}{}

\tagitem{prvEx}{%
  \indcase{!A}{\lexists[x][!B]}{$\Struct{M} \VDash
  {\indfrm}$ iff there is an $a \in \Domain M$ such that
  $\Struct{M[a/c]} \VDash \Subst{!B}{c}{x}$, if $c$ does not occur
  in~$!B$.}}{}
\end{enumerate}
Let $x_1$, \dots, $x_n$ be all free !!{variable}s in~$!A$,
$c_1$, \dots, $c_n$ constant symbols not in~$!A$,
$a_1$, \dots, $a_n \in \Domain M$, and $s(x_i) = a_i$.

Show that $\Sat{M}{!A}[s]$ iff $\Struct M[a_1/c_1,\dots,a_n/c_n]
\VDash \Subst{\Subst{!A}{c_1}{x_1}\dots}{c_n}{x_n}$.

(This problem shows that it is possible to give a semantics for
first-order logic that makes do without variable assignments.)
\end{probdeferred}
\begin{probdeferred}{5.12}
Suppose that $f$ is a function symbol not in~$!A(x,y)$. Show that
there is !!a{structure}~$\Struct{M}$ such that
$\Sat{M}{\lforall[x][\lexists[y][!A(x,y)]]}$ iff there is an~$\Struct
M'$ such that $\Sat{M'}{\lforall[x][!A(x,f(x))]}$.

(This problem is a special case of what's known as Skolem's Theorem;
$\lforall[x][!A(x,f(x))]$ is called a \emph{Skolem normal form} of
$\lforall[x][\lexists[y][!A(x,y)]]$.)
\end{probdeferred}
\begin{probdeferred}{5.13}
Carry out the proof of \olref[fol][syn][ext]{prop:extensionality} in
detail.
\end{probdeferred}
\begin{probdeferred}{5.13}
Prove \olref[fol][syn][ext]{prop:ext-formulas}
\end{probdeferred}
\begin{probdeferred}{5.14}
\begin{enumerate}
\item Show that $\Gamma \Entails \bot$ iff $\Gamma$ is unsatisfiable.
\item Show that $\Gamma \cup \{!A\} \Entails \bot$ iff $\Gamma \Entails \lnot !A$.
\item Suppose $c$ does not occur in $!A$ or $\Gamma$.  Show that
  $\Gamma \Entails \lforall[x][!A]$ iff $\Gamma \Entails
  \Subst{!A}{c}{x}$.
\end{enumerate}
\end{probdeferred}
\begin{probdeferredtag}{5.14}
{probEx,probAll}
  Complete the proof of \olref[fol][syn][sem]{prop:quant-terms}.
\end{probdeferredtag}
 \renewcommand*{\probch}{6} \setcounter{probd}{0}
\section*{Problems for Chapter~6}
\begin{probdeferred}{6.4}
Find !!{formula}s in $\Lang L_A$ which define the following relations:
\begin{enumerate}
\item $n$ is between $i$ and $j$;
\item $n$ evenly divides $m$ (i.e., $m$ is a multiple of $n$);
\item $n$ is a prime number (i.e., no number other than $1$ and $n$ evenly
  divides~$n$).
\end{enumerate}
\end{probdeferred}
\begin{probdeferred}{6.4}
Suppose the formula $!A(\Obj v_1, \Obj v_2)$ expresses the relation $R
\subseteq \Domain M^2$ in a !!{structure}~$\Struct M$.  Find formulas
that express the following relations:
\begin{enumerate}
\item the inverse $R^{-1}$ of $R$;
\item the relative product $R \mid R$;
\end{enumerate}
Can you find a way to express $R^+$, the transitive closure of~$R$?
\end{probdeferred}
\begin{probdeferred}{6.4}
Let $\Lang{L}$ be the language containing a 2-place predicate symbol
$<$ only (no other !!{constant}s, !!{function}s or !!{predicate}s---
except of course~$\eq$). Let $\Struct{N}$ be the structure such that
$\Domain{N} = \Nat$, and $\Assign{<}{N} = \Setabs{\tuple{n,m}}{n <
  m}$. Prove the following:
\begin{enumerate}
\item $\{ 0 \}$ is definable in $\Struct{N}$;
\item $\{ 1 \}$ is definable in $\Struct{N}$;
\item $\{ 2 \}$ is definable in $\Struct{N}$;
\item for each $n \in \Nat$, the set $\{ n \}$ is definable in
  $\Struct{N}$;
\item every finite subset of $\Domain{N}$ is definable in
  $\Struct{N}$;
\item every co-finite subset of $\Domain{N}$ is definable in
  $\Struct{N}$ (where $X \subseteq \Nat$ is co-finite iff
  $\Nat \setminus X$ is finite).
\end{enumerate}
\end{probdeferred}
\begin{probdeferred}{6.5}
Show that the comprehension principle is inconsistent by giving
!!a{derivation} that shows
\[
\lexists[y][\lforall[x][(x \in y \liff x \notin x)]] \Proves \lfalse.
\]
It may help to first show $(A \lif \lnot A) \land (\lnot A \lif A)
\Proves \lfalse$.
\end{probdeferred}
 \renewcommand*{\probch}{7} \setcounter{probd}{0}
\section*{Problems for Chapter~7}
\begin{probdeferred}{7.6}
Give !!{derivation}s of the following sequents:
\begin{enumerate}
\item $\Sequent \lnot(!A \lif !B) \lif (!A \land \lnot !B)$
\item $(!A \land !B) \lif !C \Sequent (!A \lif !C) \lor (!B \lif !C)$
\end{enumerate}
\end{probdeferred}
\begin{probdeferred}{7.7}
Give !!{derivation}s of the following sequents:
\begin{enumerate}
\item $\lforall[x][(!A(x) \lif !B)] \Sequent (\lexists[y][!A(y)] \lif !B)$
\item $\lexists[x][(!A(x) \lif \lforall[y][!A(y)])]$
\end{enumerate}
\end{probdeferred}
\begin{probdeferred}{7.8}
Prove \olref[fol][seq][ptn]{prop:incons}
\end{probdeferred}
\begin{probdeferred}{7.9}
Prove that $\Gamma \Proves \lnot !A$ iff $\Gamma \cup \{!A\}$ is inconsistent.
\end{probdeferred}
\begin{probdeferred}{7.12}
Complete the proof of \olref[fol][seq][sou]{thm:sequent-soundness}.
\end{probdeferred}
\begin{probdeferred}{7.13}
Give !!{derivation}s of the following sequents:
\begin{enumerate}
\item $\Sequent \lforall[x][\lforall[y][((x = y \land !A(x)) \lif !A(y))]]$
\item $\lexists[x][!A(x)] \land \lforall[y][\lforall[z][((!A(y) \land
    !A(z)) \lif y = z)]] \Sequent
\lexists[x][(!A(x) \land \lforall[y][(!A(y) \lif y = x)])]$
\end{enumerate}
\end{probdeferred}
 \renewcommand*{\probch}{8} \setcounter{probd}{0}
\section*{Problems for Chapter~8}
\begin{probdeferred}{8.3}
Complete the proof of \olref[fol][com][ccs]{prop:ccs}.
\end{probdeferred}
\begin{probdeferred}{8.6}
Complete the proof of \olref[fol][com][mod]{prop:quant-termmodel}.
\end{probdeferred}
\begin{probdeferred}{8.6}
Complete the proof of \olref[fol][com][mod]{lem:truth}.
\end{probdeferred}
\begin{probdeferred}{8.7}
Complete the proof of~\olref[fol][com][ide]{prop:approx-equiv}.
\end{probdeferred}
\begin{probdeferred}{8.8}
Use \olref[fol][com][cth]{cor:completeness} to prove
\olref[fol][com][cth]{thm:completeness}, thus showing that the two
formulations of the completeness theorem are equivalent.
\end{probdeferred}
\begin{probdeferred}{8.8}
In order for !!a{derivation} system to be complete, its rules must be
strong enough to prove every unsatisfiable set inconsistent.  Which of
the rules of !!{derivation} were necessary to prove completeness?  Are any
of these rules not used anywhere in the proof?  In order to answer
these questions, make a list or diagram that shows which of the rules
of !!{derivation} were used in which results that lead up to the proof of
\olref[fol][com][cth]{thm:completeness}.  Be sure to note any tacit
uses of rules in these proofs.
\end{probdeferred}
\begin{probdeferred}{8.9}
Prove (1) of \olref[fol][com][com]{thm:compactness}.
\end{probdeferred}
\begin{probdeferred}{8.9}
In the standard model of arithmetic~$\Struct{N}$, there is no
!!{element}~$k \in \Domain{N}$ which satisfies every formula~$\num{n}
< x$ (where $\num{n}$ is $\Obj{0}^{\prime\dots\prime}$ with $n$
$\prime$'s).  Use the compactness theorem to show that the set of
!!{sentence}s in the language of arithmetic which are true in the standard
model of arithmetic $\Struct{N}$ are also true in
!!a{structure}~$\Struct{N'}$ that contains !!a{element} which
\emph{does} satisfy every formula $\num{n} < x$.
\end{probdeferred}
\begin{probdeferred}{8.10}
Prove \olref[fol][com][cpd]{prop:fsat-ccs}. Avoid the use of $\Proves$.
\end{probdeferred}
\begin{probdeferred}{8.10}
Prove \olref[fol][com][cpd]{lem:fsat-henkin}. (Hint: The crucial step
is to show that if $\Gamma_n$ is finitely satisfiable, so is $\Gamma_n
\cup \{!D_n\}$, without any appeal to !!{derivation}s or consistency.)
\end{probdeferred}
\begin{probdeferred}{8.10}
Prove \olref[fol][com][cpd]{prop:fsat-instances}.
\end{probdeferred}
\begin{probdeferred}{8.10}
Prove \olref[fol][com][cpd]{lem:fsat-lindenbaum}.  (Hint: the crucial
step is to show that if $\Gamma_n$ is finitely satisfiable, then
either $\Gamma_n \cup \{!A_n\}$ or $\Gamma_n \cup \{\lnot !A_n\}$ is
finitely satisfiable.)
\end{probdeferred}
\begin{probdeferred}{8.10}
Write out the complete proof of the Truth Lemma
(\olref[fol][com][mod]{lem:truth}) in the version required for the
proof of \olref[fol][com][cpd]{thm:compactness-direct}.
\end{probdeferred}
\begin{probdeferred}{III}
Let $\Gamma$ be the set of all sentences $!A$ in the language of
arithmetic such that $\Sat{N}{!A}$, i.e., $\Gamma$ contains all
sentences true in the ``standard model.'' Show that there is a
model~$\Struct M$ of $\Gamma$ which is not covered, i.e., some
$a \in \Domain{M}$ is such that $a \neq \Value{t}{M}$ for all
closed terms~$t$.
\end{probdeferred}
 \renewcommand*{\probch}{9} \setcounter{probd}{0}
\section*{Problems for Chapter~9}
\begin{probdeferred}{9.4}
  Prove \olref[cmp][rec][prf]{prop:mult-pr} by showing that the
  primitive recursive definition of $\Mult$ is can be put into the
  form required by \olref[cmp][rec][prf]{defn:primitive-recursion} and
  showing that the corresponding functions $f$ and~$g$ are primitive
  recursive.
\end{probdeferred}
\begin{probdeferred}{9.5}
  Give the complete primitive recursive notation for~$\Mult$.
\end{probdeferred}
\begin{probdeferred}{9.7}
  Prove \olref[cmp][rec][exa]{prop:min-pr}.
\end{probdeferred}
\begin{probdeferred}{9.7}
Show that \[
f(x, y) =
2^{(2^{\iddots^{2^{x}}})}\raisebox{1ex}{\bigg\rbrace}
\raisebox{1ex}{\text {$y$ $2$'s}}\] is primitive recursive.
\end{probdeferred}
\begin{probdeferred}{9.7}
Show that integer division $d(x, y) = \lfloor x/y \rfloor$ (i.e.,
division, where you disregard everything after the decimal point) is
primitive recursive. When $y = 0$, we stipulate $d(x, y) = 0$. Give an
explicit definition of~$d$ using primitive recursion and
composition.
\end{probdeferred}
\begin{probdeferred}{9.9}
Suppose $R(\vec x, z)$ is primitive recursive. Define the function
$m'_R(\vec{x}, y)$ which returns the least~$z$ less than~$y$ such that
$R(\vec{x}, z)$ holds, if there is one, and $0$ otherwise, by
primitive recursion from~$\Char{R}$.
\end{probdeferred}
\begin{probdeferred}{9.10}
Define integer division $d(x, y)$ using bounded minimization.
\end{probdeferred}
\begin{probdeferred}{9.11}
Show that there is a primitive recursive function~$\fn{sconcat}(s)$
with the property that
\[
\fn{sconcat}(\tuple{s_0, \dots, s_k}) = s_0 \concat \dots \concat s_k.
\]
\end{probdeferred}
\begin{probdeferred}{9.11}
Show that there is a primitive recursive function~$\fn{tail}(s)$
with the property that
\begin{align*}
  \fn{tail}(\emptyseq) & = 0 \text{ and}\\
  \fn{tail}(\tuple{s_0, \dots, s_{k}}) & = \tuple{s_1, \dots, s_{k}}.
\end{align*}
\end{probdeferred}
\begin{probdeferred}{9.11}
  Prove \olref[cmp][rec][seq]{prop:subseq}.
\end{probdeferred}
\begin{probdeferred}{9.12}
  The definition of $\fn{hSubtreeSeq}$ in the proof of
  \olref[cmp][rec][tre]{prop:subtreeseq} in general includes
  repetitions. Give an alternative definition which guarantees that
  the code of a subtree occurs only once in the resulting list.
\end{probdeferred}
 \renewcommand*{\probch}{10} \setcounter{probd}{0}
\section*{Problems for Chapter~10}
\begin{probdeferred}{10.3}
Show that the function $\fn{flatten}(z)$, which turns the sequence
$\tuple{\Gn{t_1}, \dots, \Gn{t_n}}$ into $\Gn{t_1, \dots, t_n}$, is
primitive recursive.
\end{probdeferred}
\begin{probdeferred}{10.4}
Give a detailed proof of \olref[inc][art][frm]{prop:frm-primrec} along
the lines of the first proof of
\olref[inc][art][trm]{prop:term-primrec}
\end{probdeferred}
\begin{probdeferred}{10.4}
Give a detailed proof of \olref[inc][art][frm]{prop:frm-primrec} along
the lines of the alternate proof of
\olref[inc][art][trm]{prop:term-primrec}
\end{probdeferred}
\begin{probdeferred}{10.4}
Prove \olref[inc][art][frm]{prop:freeocc-primrec}.  You may make use
of the fact that any substring of !!a{formula} which is !!a{formula}
is a sub-!!{formula} of it.
\end{probdeferred}
\begin{probdeferred}{10.5}
Prove \olref[inc][art][sub]{prop:free-for}
\end{probdeferred}
\begin{probdeferred}{10.6}
  Define the following properties as in
  \olref[inc][art][plk]{prop:followsby}:
  \begin{enumerate}
  \item $\fn{FollowsBy}_{\Cut}(p)$,
  \item $\fn{FollowsBy}_{\LeftR\lif}(p)$,
  \item $\fn{FollowsBy}_{\eq}(p)$,
  \item $\fn{FollowsBy}_{\RightR{\lforall}}(p)$.
  \end{enumerate}
  For the last one, you will have to also show that you can test
  primitive recursively if the last inference of the
  !!{derivation} with G\"odel number~$p$ satisfies the eigenvariable
  condition, i.e., the eigenvariable~$a$ of the $\RightR{\lforall}$
  does not occur in the end-sequent.
  
\end{probdeferred}
 \renewcommand*{\probch}{11} \setcounter{probd}{0}
\section*{Problems for Chapter~11}
\begin{probdeferred}{11.5}
Prove that $\eq[y][\Obj 0]$, $\eq[y][x']$, and $\eq[y][x_i]$ represent
$\Zero$, $\Succ$, and $\Proj{n}{i}$, respectively.
\end{probdeferred}
\begin{probdeferred}{11.5}
Prove \olref[inc][req][bre]{lem:q-proves-mult}.
\end{probdeferred}
\begin{probdeferred}{11.5}
Use \olref[inc][req][bre]{lem:q-proves-mult} to prove
\olref[inc][req][bre]{prop:rep-mult}.
\end{probdeferred}
\begin{probdeferred}{11.6}
Using the proofs of \olref[inc][req][cmp]{prop:rep2} and
\olref[inc][req][cmp]{prop:rep2} as a guide, carry out the proof of
\olref[inc][req][cmp]{prop:rep-composition} in detail.
\end{probdeferred}
\begin{probdeferred}{11.9}
Show that if $R$ is representable in~$\Th{Q}$, so is~$\Char{R}$.
\end{probdeferred}
 \renewcommand*{\probch}{12} \setcounter{probd}{0}
\section*{Problems for Chapter~12}
\begin{probdeferred}{12.3}
  Every $\omega$-consistent theory is consistent. Show that the
  converse does not hold, i.e., that there are consistent but
  $\omega$-inconsistent theories. Do this by showing that $\Th{Q} \cup
  \{\lnot !G_\Th{Q}\}$ is consistent but $\omega$-inconsistent.
\end{probdeferred}
\begin{probdeferred}{12.7}
Show that $\Th{PA}$ !!{derive}s $!G_{\Th{PA}} \lif \OCon[\Th{PA}]$.
\end{probdeferred}
\begin{probdeferred}{12.8}
Let $\Th{T}$ be a computably axiomatized theory, and
let $\OProv[T]$ be !!a{derivability} predicate for $\Th{T}$. Consider the
following four statements:
\begin{enumerate}
\item If $T \Proves !A$, then $T \Proves \OProv[T](\gn{!A})$.
\item $T \Proves !A \lif \OProv[T](\gn{!A})$.
\item If $T \Proves \OProv[T](\gn{!A})$, then $T \Proves !A$.
\item $T \Proves \OProv[T](\gn{!A}) \lif !A$
\end{enumerate}
Under what conditions are each of these statements true?
\end{probdeferred}
\begin{probdeferred}{12.9}
Show that $Q(n) \defiff n \in \Setabs{\Gn{!A}}{\Th{Q} \Proves !A}$ is
  definable in arithmetic.
\end{probdeferred}
